# Imports

import time
import project_functions
import numpy as np
from ray import tune
import pandas as pd
import matplotlib.pyplot as plt
from IPython.display import display, Markdown
from neuralforecast import NeuralForecast
import pytorch_lightning as pl
from neuralforecast.losses.pytorch import MQLoss
from neuralforecast.tsdataset import TimeSeriesDataset
from neuralforecast.models import TFT
from neuralforecast.auto import AutoTFT
import torch
import json
import pickle
import os
import hyperopt
os.environ["PYTORCH_CUDA_ALLOC_CONF"] = 'max_split_size_mb:512'

# Variables

with open("/data/wiay/2386142c/msci_project/condor_jobs/scripts/variables.json", "r") as f:
    vars = json.load(f)

loss = MQLoss(level=vars["loss_levels"])

output_name = "/data/wiay/2386142c/msci_project/condor_jobs/automatic_outputs/{}.csv".format(time.strftime("%Y-%m-%d-%H_%M_%S"))


# Import processed data

all_df, training_df = project_functions.import_data_from_pickle(name=vars["processed_data_name"])

weather_columns = vars["weather_columns"]

past_exog = [[i] for i in weather_columns] + [None]

futr_exog = [[i, "day_of_week_sin", "day_of_week_cos"] for i in weather_columns] + ["day_of_week_sin", "day_of_week_cos"]


# Define search space

config = {
    "input_size": tune.randint(6, 200),
    "hidden_size": tune.choice([i for i in range(100, 1000, 20)]),
    "max_epochs": tune.randint(200, 1000),
    "hist_exog_list": tune.choice(past_exog),
    "futr_exog_list": tune.choice(futr_exog),
    "learning_rate": tune.loguniform(1e-5, 1e-1),
    "windows_batch_size": tune.choice([32, 64, 128, 256, 512, 1024])
}


# Define and train model

model = AutoTFT(h=vars["output_size"],
                config=config,
                loss=loss,
                num_samples=1000)

fcst = NeuralForecast(
    models=[model],
    freq=vars["freq"]
)

fcst.fit(df=training_df, val_size=vars["val_size"])


# Save model

results = model.results.get_dataframe()

results.to_csv(output_name)