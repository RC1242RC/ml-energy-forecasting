# Imports

import time
import project_functions
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from IPython.display import display, Markdown
from neuralforecast import NeuralForecast
import pytorch_lightning as pl
from neuralforecast.losses.pytorch import MQLoss
from neuralforecast.tsdataset import TimeSeriesDataset
from neuralforecast.models import TFT
import torch
import json
import pickle
import os
import wandb
os.environ["PYTORCH_CUDA_ALLOC_CONF"] = 'max_split_size_mb:512'

# Variables

with open("variables.json", "r") as f:
    vars = json.load(f)


# Weights and biases tracking

#wandb.login()
#run = wandb.init(project="testing", reinit=True, config={   "processed_data_name": vars["processed_data_name"],
#                                                            "input_size": vars["input_size"],
#                                                            "output_size": vars["output_size"],
#                                                            "layers": vars["layers"],
#                                                            "epochs": vars["epochs"],
#                                                            "windows_batch_size": vars["windows_batch_size"],
#                                                            "past_exogenous_vars": vars["past_exogenous_vars"],
#                                                            "future_exogenous_vars": vars["future_exogenous_vars"],
#                                                            "learning_rate": vars["learning_rate"],
#                                                            "loss_levels": vars["loss_levels"],
#                                                            "freq": vars["freq"],
#                                                            "val_size": vars["val_size"],
#                                                            "val_check_steps": vars["val_check_steps"],
#                                                        })


print(1)
loss = MQLoss(level=vars["loss_levels"])

print(2)
model_name = "../models/{}.pt".format(time.strftime("%Y-%m-%d-%H_%M_%S"))


# Import processed data

print(3)
all_df, training_df = project_functions.import_data_from_pickle(name=vars["processed_data_name"])


# Define and train model

print(4)
fcst = NeuralForecast(
    models=[TFT(h=vars["output_size"],
                input_size=vars["input_size"],
                hidden_size=vars["layers"],
                max_epochs=vars["epochs"],
                hist_exog_list=vars["past_exogenous_vars"],
                futr_exog_list=vars["future_exogenous_vars"],
                learning_rate=vars["learning_rate"],
                windows_batch_size=vars["windows_batch_size"],
                scaler_type='robust',
                val_check_steps=vars["val_check_steps"],
                loss=loss,
                enable_progress_bar=True,

                step_size=vars["step_size"],
                valid_loss=loss),
    ],
    freq=vars["freq"]
)

print(5)
fcst.fit(df=training_df, val_size=vars["val_size"])


# Save model

print(6)
fcst.save(path=model_name)

#run.finish()