import pandas as pd
import numpy as np
import time
import pickle
import json


def import_one_series(usage_file, usage_column, weather_file, weather_columns):
  usage_df = pd.read_csv(usage_file)
  usage_df = usage_df[[usage_column]]
  usage_df = usage_df.rename(columns={usage_column: "y"})
  usage_df["y"] = (usage_df["y"] - usage_df.mean()["y"])/usage_df.std()["y"]
  usage_df["unique_id"] = usage_column
  usage_df["ds"] = [pd.Timestamp(701280+x, unit="h") for x in usage_df.index]
  usage_df["day_of_week_sin"] = [np.sin((2*np.pi*x.day_of_week)/6) for x in usage_df["ds"]]
  usage_df["day_of_week_cos"] = [np.cos((2*np.pi*x.day_of_week)/6) for x in usage_df["ds"]]

  weather_df = pd.read_csv(weather_file)
  weather_df_select = weather_df[[i for i in weather_columns]]
  weather_df_norm = (weather_df_select - weather_df_select.mean())/weather_df_select.std()

  data_df = pd.concat([usage_df, weather_df_norm], axis=1)

  #print("Head:")
  #print(data_df.head())
  #print("Describe:")
  #print(data_df.describe())
  
  return(data_df)

def arrange_multiple_series(usage_file, usage_column, weather_file, weather_columns, training_data_fraction, training_data_series):
  print("Importing data {} and {}".format(usage_file, weather_file))
  all_df = 0
  training_df = 0
  for column in usage_column:
    temp = import_one_series(usage_file, column, weather_file, weather_columns)
    if type(all_df) == int:
      all_df = temp
    else:
      all_df = pd.concat([all_df, temp])
    if column in training_data_series:
      length = len(temp)
      cutoff = int(length*training_data_fraction)
      temp2 = temp[:cutoff]
      if type(training_df) == int:
        training_df = temp2
      else:
        training_df = pd.concat([training_df, temp2])
  print("Data has length {}. Using {} for training.".format(length, cutoff))
  return(all_df, training_df)

def save_data_to_pickle(all_df, training_df):
  all_df_name = "/data/wiay/2386142c/msci_project/condor_jobs/data/processed_data/{}ADF.pck".format(time.strftime("%Y-%m-%d-%H_%M_%S"))
  training_df_name = "/data/wiay/2386142c/msci_project/condor_jobs/data/processed_data/{}TDF.pck".format(time.strftime("%Y-%m-%d-%H_%M_%S"))
  with open(all_df_name, "wb") as f:
    pickle.dump(all_df, f)
  with open(training_df_name, "wb") as f:
    pickle.dump(training_df, f)

def import_data_from_pickle(name):
  with open("/data/wiay/2386142c/msci_project/condor_jobs/data/processed_data/{}ADF.pck".format(name), "rb") as f:
    all_df = pickle.load(f)
  with open("/data/wiay/2386142c/msci_project/condor_jobs/data/processed_data/{}TDF.pck".format(name), "rb") as f:
    training_df = pickle.load(f)
  return(all_df, training_df)

def make_and_quantify_prediction(model, predict_df, input_size, output_size):
  known_df = 0
  future_df = 0
  for start in range(0, len(predict_df)-(output_size + input_size)):
    d_1 = start
    d_2 = start + input_size
    d_3 = d_2 + output_size
    known_temp = predict_df.copy()[d_1: d_2]
    known_temp["unique_id"] = str(start)
    future_temp = predict_df.copy()[d_2: d_3]
    future_temp["unique_id"] = str(start)
    if type(known_df) ==  int:
      known_df = known_temp
      future_df = future_temp
    else: 
      known_df = pd.concat([known_df, known_temp])
      future_df = pd.concat([future_df, future_temp])
  prediction = model.predict(df=known_df, futr_df=future_df).reset_index()
  temp = predict_df.copy()[["ds", "y"]]
  prediction = prediction.merge(temp, on="ds", how="outer")
  prediction["q"] = np.where((prediction["y"] - prediction["TFT-median"]) > 0, 
                             (prediction["y"] - prediction["TFT-median"])/(prediction["TFT-hi-90"] - prediction["TFT-median"]),
                             - (prediction["y"] - prediction["TFT-median"])/(prediction["TFT-lo-90"] - prediction["TFT-median"]))
  prediction["err"] = prediction["TFT-median"] - prediction["y"]
  qs = pd.DataFrame(columns = ["max_q", "avg_q", "max_err", "avg_err", "all_q", "all_err", "ds", "y", "median", "hi", "lo"])
  for id in range(0, len(predict_df)-(output_size + input_size)):
    temp = prediction.copy()
    temp = temp[temp["unique_id"] == str(id)]
    q_l = temp["q"].tolist()
    err_l = temp["err"].tolist()
    qs.loc[str(id)] = [max(np.abs(q_l)), sum(np.abs(q_l))/len(q_l), max(np.abs(err_l)), sum(np.abs(err_l))/len(err_l), q_l, err_l, temp["ds"].tolist()[0] - pd.Timedelta(1, unit="h"), temp["y"].tolist(), temp["TFT-median"].tolist(), temp["TFT-hi-90"].tolist(), temp["TFT-lo-90"].tolist()]
  
  return(qs)

def q_squared_func(median, level_above, level_below, x):
  if (x-median) > 0:
    return(((x-median)/(level_above-median))**2)
  else:
    return(-((x-median)/(level_below-median))**2)

def q_linear_func(median, level_above, level_below, x):
  if (x-median) > 0:
    return((x-median)/(level_above-median))
  else:
    return(-(x-median)/(level_below-median))
