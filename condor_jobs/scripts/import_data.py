import project_functions
import pickle
import json

# Variables

with open("variables.json", "r") as f:
    vars = json.load(f)


# Import, arrange, and save data

all_df, training_df = project_functions.arrange_multiple_series(vars["usage_file"], 
                                              vars["usage_column"], 
                                              vars["weather_file"], 
                                              vars["weather_columns"], 
                                              vars["training_data_fraction"], 
                                              vars["training_data_series"])

print(all_df.head())
print(all_df.tail())
print(all_df.describe())
print(all_df.corr())

print(training_df.head())
print(training_df.tail())
print(training_df.describe())
print(training_df.corr())

project_functions.save_data_to_pickle(all_df, training_df)