# Imports

from neuralforecast import NeuralForecast
import matplotlib.pyplot as plt
import import_data
import json


# Variables

with open("variables.json", "r") as f:
    vars = json.load(f)

model_name = vars["model_name"]
model_path = "../models/{}.pt".format(model_name)
fig_path = "../plots/{}.png".format(model_name)
start_point = vars["start_point"]
end_point = vars["end_point"]
show_previous = vars["show_previous"]
input_size = vars["input_size"]
output_size = vars["output_size"]


# Load model

print("Loading model {}".format(model_name))
fcst = NeuralForecast.load(path=model_path)

# Import processed data

all_df, training_df = import_data.import_data_from_pickle(name=vars["processed_data_name"])


# Forecast

#forecast = fcst.predict(df=test_df[start_point:start_point+input_size], futr_df=test_df[start_point+input_size:start_point+input_size+output_size])


# Plot

fig, ax = plt.subplots(len(vars["prediction_series"]), 1, figsize=(20,7))

for i in range(len(vars["prediction_series"])):

    temp_df = all_df.loc[all_df["unique_id"]==vars["prediction_series"]]
    temp_df = temp_df[]
    loss1_df = temp_df[input_size:]
    loss2_df = 
    ax[i].plot(temp_df["ds"], temp_df["y"])



#ax.plot(test_df["ds"][start_point+input_size-show_previous:start_point+input_size+output_size], test_df["y"][start_point+input_size-show_previous:start_point+input_size+output_size], c="black", label="True")

#ax.plot(forecast['ds'], forecast['TFT-median'], label="Median")
#ax.fill_between(x=forecast["ds"], y1=forecast['TFT-lo-90.0'], y2=forecast['TFT-hi-90.0'],alpha=0.4, label="90")

fig.legend()
plt.savefig(fig_path)
