# Imports

from neuralforecast import NeuralForecast
import matplotlib.pyplot as plt
import project_functions
import json
import scipy
import numpy as np


# Variables

with open("variables.json", "r") as f:
    vars = json.load(f)

model_name = vars["model_name"]
model_path = "../models/{}.pt".format(model_name)
fig_path = "../plots/{}_QUANTIFY2.png".format(model_name)


# Load model

print("Loading model {}".format(model_name))
fcst = NeuralForecast.load(path=model_path)

# Import processed data

all_df, training_df = project_functions.import_data_from_pickle(name=vars["processed_data_name"])

all_df.corr().to_csv("all_df_correlation.csv")

predict_df = all_df[all_df["unique_id"] == vars["prediction_series"]][vars["start_point"]:vars["end_point"]]

qs = project_functions.make_and_quantify_prediction(model=fcst, predict_df=predict_df, input_size=vars["input_size"], output_size=vars["output_size"])

trimmed_predict_df = predict_df[vars["input_size"]:-vars["output_size"]]

fig, ax = plt.subplots(2,2, figsize=(15,10))

ax[0,0].set_title("Absolute quantile errors for prediction window")
ax[0,0].scatter(qs["ds"], qs["max_q"], c="b", label="Maximum error")
ax[0,0].plot(qs["ds"], qs["avg_q"], c="r", label="Average error")

ax[0,1].set_title("Absolute errors for prediction window")
ax[0,1].scatter(qs["ds"], qs["max_err"], c="b", label="Maximum error")
ax[0,1].plot(qs["ds"], qs["avg_err"], c="r", label="Average error")

all_qs = np.array(qs["all_q"].tolist()).flatten()
_, bins, _ = ax[1,0].hist(all_qs, 300, density=1, alpha=0.5)
mu, sigma = scipy.stats.norm.fit(all_qs)
best_fit_line = scipy.stats.norm.pdf(bins, mu, sigma)
abs_qs = [abs(i) for i in all_qs]
abs_avg = sum(abs_qs)/len(abs_qs)

ax[1,0].plot(bins, best_fit_line, label="Avg: {:.3g}, SD: {:.3g}, abs avg: {:.3g}".format(mu, sigma, abs_avg))
ax[1,0].set_xlim([-sigma*5,sigma*5])

all_errs = np.array(qs["all_err"].tolist()).flatten()
_, bins2, _ = ax[1,1].hist(all_errs, 300, density=1, alpha=0.5)
mu2, sigma2 = scipy.stats.norm.fit(all_errs)
best_fit_line2 = scipy.stats.norm.pdf(bins2, mu2, sigma2)
abs_errs = [abs(i) for i in all_errs]
abs_avg2 = sum(abs_errs)/len(abs_errs)

ax[1,1].plot(bins2, best_fit_line2, label="Avg: {:.3g}, SD: {:.3g}, abs avg: {:.3g}".format(mu2, sigma2, abs_avg2))
ax[1,1].set_xlim([-sigma2*5,sigma2*5])


#plt.plot(predict_df["ds"][0:200], predict_df["y"][0:200])
#plt.plot(test["ds"], test["TFT-median"])
#plt.fill_between(test["ds"], test["TFT-lo-90.0"], test["TFT-hi-90.0"])

for a in ax.flatten():
  a.legend()
plt.suptitle(fig_path)
plt.savefig(fig_path)