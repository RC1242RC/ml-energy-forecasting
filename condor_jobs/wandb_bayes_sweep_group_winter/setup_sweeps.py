import pickle
import yaml
import wandb

best_vars = ["7", "17", "24", "16", "13", "22", "9", "8", "11", "14", "23"]
var = []
for n in best_vars:
    if len(var) == 0:
        var += [[n]]
    else:
        var += [var[-1]+ [n]]
var = var[1:]

folder = "wandb_bayes_sweep_group_winter"
sweeps = []
its = 0

with open("wandb_sweep_configs.yaml", "r") as f:
    lines = f.readlines()

for v in var:
    v_str = "-".join(v)
    l = lines.copy()
    l[0] = "program: /data/wiay/2386142c/msci_project/condor_jobs/{}/bayes_fullsweep_tft.py\n".format(folder)
    l[1] = "name: \"{}\"\n".format(v_str)
    l[12] = "    value: {}\n".format(v_str)
    l[14] = "    value: {}\n".format(folder)
    if v == "0":
        l[79] = "    value: []\n"
    else:
        l[79] = "    value: {}\n".format(v)
    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/wandb_sweep_configs{}.yaml".format(folder, v_str), "w") as f:
        f.writelines(l)
    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/wandb_sweep_configs{}.yaml".format(folder, v_str), "r") as f:
        config = yaml.load(f, Loader=yaml.Loader)
    sweepid = wandb.sweep(sweep=config, project=folder)
    sweeps += ["{}\n".format(sweepid)]

    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/its{}".format(folder, v_str), "wb") as f:
        pickle.dump(its, f)
    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/sweepid{}".format(folder, v_str), "wb") as f:
        pickle.dump(sweepid, f)

with open("sweeps.txt", "w") as f:
    f.writelines(sweeps)