# Imports

import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from IPython.display import display, Markdown
from neuralforecast import NeuralForecast
import pytorch_lightning as pl
from neuralforecast.losses.pytorch import MQLoss
from neuralforecast.tsdataset import TimeSeriesDataset
from neuralforecast.models import TFT
import torch
import json
import pickle
import os
import sys
import wandb
os.environ["PYTORCH_CUDA_ALLOC_CONF"] = 'max_split_size_mb:512'
os.environ["WANDB_AGENT_DISABLE_FLAPPING"] = "true"
os.environ["WANDB_AGENT_MAX_INITIAL_FAILURES"]= "20"
sys.path.append("/data/wiay/2386142c/msci_project/condor_jobs/scripts")
print(torch.cuda.get_device_name(0))
import project_functions

# Variables

json_file = sys.argv[1]
#json_file = "./"+sys.argv[1]
with open(json_file, "r") as f:
    vars = json.loads(f.read())

past = vars["weather_exog_list"]
future = ["day_of_week_sin", "day_of_week_cos", "month_sin", "month_cos", "hour_sin", "hour_cos"] + vars["weather_exog_list"]

# Iterations

with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/its{}".format(vars["folder"], vars["var"]), "rb") as f:
    its = pickle.load(f)
with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/sweepid{}".format(vars["folder"], vars["var"]), "rb") as f:
    sweepid = pickle.load(f)

if its > 100:
    os.system("wandb sweep --stop r-clark1242/{}/{}".format(vars["folder"], sweepid))
else:
    its += 1
    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/its{}".format(vars["folder"], vars["var"]), "wb") as f:
        pickle.dump(its, f)




# Weights and biases tracking

wandb.login()
run = wandb.init(project="wandb_bayes_sweep_group_winter", reinit=True, config={   "processed_data_name": vars["processed_data_name"],
                                                            "input_size": vars["input_size"],
                                                            "output_size": vars["output_size"],
                                                            "layers": vars["layers"],
                                                            "epochs": vars["epochs"],
                                                            "windows_batch_size": vars["windows_batch_size"],
                                                            "past_exogenous_vars": past,
                                                            "future_exogenous_vars": future,
                                                            "learning_rate": vars["learning_rate"],
                                                            "loss_levels": vars["loss_levels"],
                                                            "freq": vars["freq"],
                                                            "val_size": vars["val_size"],
                                                            "val_check_steps": vars["val_check_steps"],
                                                            "step_size": vars["step_size"],
                                                        })



loss = MQLoss(level=vars["loss_levels"])


# Import processed data

all_df, training_df = project_functions.import_data_from_pickle(name=vars["processed_data_name"])


# Define and train model

fcst = NeuralForecast(
    models=[TFT(h=vars["output_size"],
                input_size=vars["input_size"],
                hidden_size=vars["layers"],
                max_epochs=vars["epochs"],
                hist_exog_list=past,
                futr_exog_list=future,
                learning_rate=vars["learning_rate"],
                windows_batch_size=vars["windows_batch_size"],
                scaler_type='robust',
                val_check_steps=vars["val_check_steps"],
                loss=loss,
                valid_loss=loss,
                step_size=vars["step_size"],
                dropout=vars["dropout"],
                attn_dropout=vars["attn_dropout"],
                enable_progress_bar=True),
    ],
    freq=vars["freq"]
)

fcst.fit(df=training_df, val_size=vars["val_size"])


#model_name = "/data/wiay/2386142c/msci_project/condor_jobs/{}/best_models/{}.pt".format(vars["folder"], vars["var"])

#fcst.save(path=model_name)

# Finish

run.finish()