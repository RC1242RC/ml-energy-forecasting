import pickle
import yaml
import wandb

var = ["0", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
folder = "wandb_bayes_sweep_individual_winter"
sweeps = []
its = 0

with open("wandb_sweep_configs.yaml", "r") as f:
    lines = f.readlines()

for v in var:
    l = lines.copy()
    l[0] = "program: /data/wiay/2386142c/msci_project/condor_jobs/{}/bayes_fullsweep_tft.py\n".format(folder)
    l[1] = "name: \"{}\"\n".format(v)
    l[12] = "    value: {}\n".format(v)
    l[14] = "    value: {}\n".format(folder)
    if v == "0":
        l[79] = "    value: []\n"
    else:
        l[79] = "    value: [\"{}\"]\n".format(v)
    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/wandb_sweep_configs{}.yaml".format(folder, v), "w") as f:
        f.writelines(l)
    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/wandb_sweep_configs{}.yaml".format(folder, v), "r") as f:
        config = yaml.load(f, Loader=yaml.Loader)
    sweepid = wandb.sweep(sweep=config, project=folder)
    sweeps += ["{}\n".format(sweepid)]

    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/its{}".format(folder, v), "wb") as f:
        pickle.dump(its, f)
    with open("/data/wiay/2386142c/msci_project/condor_jobs/{}/sweepid{}".format(folder, v), "wb") as f:
        pickle.dump(sweepid, f)

with open("sweeps.txt", "w") as f:
    f.writelines(sweeps)