# Imports

import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from IPython.display import display, Markdown
from neuralforecast import NeuralForecast
import pytorch_lightning as pl
from neuralforecast.losses.pytorch import MQLoss
from neuralforecast.tsdataset import TimeSeriesDataset
from neuralforecast.models import TFT
import torch
import json
import pickle
import os
import sys
import wandb
os.environ["PYTORCH_CUDA_ALLOC_CONF"] = 'max_split_size_mb:512'
sys.path.append("/data/wiay/2386142c/msci_project/condor_jobs/scripts")
import project_functions

# Variables

#json_file = sys.argv[0]
json_file = sys.argv[1]
with open(json_file, "r") as f:
    vars = json.loads(f.read())

past = vars["weather_exog_list"]
future = ["day_of_week_sin", "day_of_week_cos"] + vars["weather_exog_list"]


# Weights and biases tracking

wandb.login()
run = wandb.init(project="bayes_fullsweep", reinit=True, config={   "processed_data_name": vars["processed_data_name"],
                                                            "input_size": vars["input_size"],
                                                            "output_size": vars["output_size"],
                                                            "layers": vars["layers"],
                                                            "epochs": vars["epochs"],
                                                            "windows_batch_size": vars["windows_batch_size"],
                                                            "past_exogenous_vars": past,
                                                            "future_exogenous_vars": future,
                                                            "learning_rate": vars["learning_rate"],
                                                            "loss_levels": vars["loss_levels"],
                                                            "freq": vars["freq"],
                                                            "val_size": vars["val_size"],
                                                            "val_check_steps": vars["val_check_steps"],
                                                        })



loss = MQLoss(level=vars["loss_levels"])

model_name = "../models/{}.pt".format(time.strftime("%Y-%m-%d-%H_%M_%S"))


# Import processed data

all_df, training_df = project_functions.import_data_from_pickle(name=vars["processed_data_name"])


# Define and train model

fcst = NeuralForecast(
    models=[TFT(h=vars["output_size"],
                input_size=vars["input_size"],
                hidden_size=vars["layers"],
                max_epochs=vars["epochs"],
                hist_exog_list=past,
                futr_exog_list=future,
                learning_rate=vars["learning_rate"],
                windows_batch_size=vars["windows_batch_size"],
                scaler_type='robust',
                val_check_steps=vars["val_check_steps"],
                loss=loss,
                enable_progress_bar=True),
    ],
    freq=vars["freq"]
)

fcst.fit(df=training_df, val_size=vars["val_size"])


# Finish

run.finish()