import pickle

var = ["0", "7", "8", "9", "10", "11", "12", "13", "14"]
#var = ["0", "7"]

with open("sweeps.txt", "r") as f:
    lines = f.readlines()

its = 0

for x in range(len(lines)):
    with open("/data/wiay/2386142c/msci_project/condor_jobs/wandb_bayes_sweep_individual_1/its{}".format(var[x]), "wb") as f:
        pickle.dump(its, f)
    with open("/data/wiay/2386142c/msci_project/condor_jobs/wandb_bayes_sweep_individual_1/sweepid{}".format(var[x]), "wb") as f:
        pickle.dump(lines[x], f)