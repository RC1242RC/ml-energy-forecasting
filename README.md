# ML Energy Forecasting
### Author: Ross Clark

This is the repository for my final year MSci project "Machine learning models for fast heat demand forecasting". 

Additional CoLab notebooks can be found [here](https://colab.research.google.com/drive/1AhHJiIHGmx8fDEkT6ahXcgyracOaGvhV?usp=sharing).